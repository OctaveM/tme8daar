# Generated by Django 3.1.4 on 2020-12-07 09:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mytig', '0003_produitdisponible'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='produitdisponible',
            options={'ordering': ('tigID',)},
        ),
        migrations.RemoveField(
            model_name='produitdisponible',
            name='availability',
        ),
    ]
